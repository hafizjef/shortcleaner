﻿Imports Microsoft.Win32
Imports System.IO

Public Class MainWindows
    Dim DetectedFileNum, TotalDetectedNum As Integer
    Dim TotalDetectedList As String

    Sub resetStatistic()
        detectedList.Items.Clear()
        DetectedFileNum = 0
        TotalDetectedNum = 0
        TotalDetectedList = ""
    End Sub

    Private Sub MainWindows_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Init()

    End Sub

    Function ScanFiles(drivePath As String, fileToScan As String)
        Dim fileType = fileToScan
        If Directory.Exists(drivePath) Then
            For Each fileToScan In Directory.GetFiles(drivePath, fileType)
                TotalDetectedList += fileToScan + vbCrLf
                detectedList.Items.Add(fileToScan)
                detectedList.SetSelected(DetectedFileNum, True)
                DetectedFileNum += 1
                TotalDetectedNum += 1
            Next
            For Each _folder As String In Directory.GetDirectories(drivePath)
                ScanFiles(_folder, fileType)
            Next

        Else

        End If
        Return 0
    End Function

    Sub CleanFiles()
        For Each item As Object In detectedList.SelectedItems
            MessageBox.Show(item)
        Next
        BtnScan.Focus()
        'BtnScan.PerformClick()
    End Sub


    Function getDriveSize(Bytes) As String
        If Bytes >= 1073741824 Then
            Return Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
        ElseIf Bytes >= 1048576 Then
            Return Format(Bytes / 1024 / 1024, "#0.00") & " MB"
        ElseIf Bytes >= 1024 Then
            Return Format(Bytes / 1024, "#0.00") & " KB"
        ElseIf Bytes < 1024 Then
            Return Fix(Bytes) & " Bytes"
        End If
        Return "0 Bytes"
    End Function

    Sub Init()
        ComboBox1.DataSource = Nothing 'Clear combobox dropdown list
        Dim comboSource As New Dictionary(Of String, String)()
        Dim totalDev = 0
        For Each drive In Environment.GetLogicalDrives
            Dim Driver As DriveInfo = New DriveInfo(drive)
            If Driver.DriveType = DriveType.Removable Then
                totalDev += 1
                comboSource.Add(drive, String.Format(String.Format("{0} ({1}) [{2}]", Driver.VolumeLabel, Driver.RootDirectory, getDriveSize(Driver.TotalSize))))
            End If
        Next
        If comboSource.Count > 0 Then
            ComboBox1.DataBindings.Clear()
            ComboBox1.DataSource = New BindingSource(comboSource, Nothing)
            ComboBox1.DisplayMember = "Value"
            ComboBox1.ValueMember = "Key"
            If totalDev < 2 Then
                AppStatus.Text = totalDev.ToString + " Device Found"
            Else
                AppStatus.Text = totalDev.ToString + " Devices Found"
            End If
            BtnScan.Enabled = True
            GroupBox1.Enabled = True
            detectedList.Enabled = True
            BtnClean.Enabled = True
        Else
            ComboBox1.DataBindings.Clear()
            BtnScan.Enabled = False
            GroupBox1.Enabled = False
            detectedList.Enabled = False
            BtnClean.Enabled = False
            AppStatus.Text = "No Device Found"
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BtnScan.Click
        Dim key As String = DirectCast(ComboBox1.SelectedItem, KeyValuePair(Of String, String)).Key
        Dim value As String = DirectCast(ComboBox1.SelectedItem, KeyValuePair(Of String, String)).Value

        resetStatistic()

        If RemAutoruns.Checked = True Then
            ScanFiles(key, "*.ini")
        End If
        If RemVBS.Checked = True Then
            ScanFiles(key, "*.vbs")
        End If
        If UnhideChk.Checked = True Then

        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles BtnClean.Click
        CleanFiles()
    End Sub

    Private Sub RefreshDeviceListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshDeviceListToolStripMenuItem.Click
        Init()
    End Sub

    Private Sub ShowHiddenFilesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowHiddenFilesToolStripMenuItem.Click

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Init()
    End Sub
End Class
