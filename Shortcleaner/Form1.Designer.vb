﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainWindows
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainWindows))
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DeviceLabel = New System.Windows.Forms.Label()
        Me.BtnScan = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.UnhideChk = New System.Windows.Forms.CheckBox()
        Me.RemVBS = New System.Windows.Forms.CheckBox()
        Me.RemAutoruns = New System.Windows.Forms.CheckBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.AppStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.detectedList = New System.Windows.Forms.ListBox()
        Me.BtnClean = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshDeviceListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowHiddenFilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(12, 47)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBox1.Size = New System.Drawing.Size(260, 21)
        Me.ComboBox1.TabIndex = 0
        '
        'DeviceLabel
        '
        Me.DeviceLabel.AutoSize = True
        Me.DeviceLabel.Location = New System.Drawing.Point(12, 31)
        Me.DeviceLabel.Name = "DeviceLabel"
        Me.DeviceLabel.Size = New System.Drawing.Size(41, 13)
        Me.DeviceLabel.TabIndex = 1
        Me.DeviceLabel.Text = "Device"
        '
        'BtnScan
        '
        Me.BtnScan.Location = New System.Drawing.Point(142, 392)
        Me.BtnScan.Name = "BtnScan"
        Me.BtnScan.Size = New System.Drawing.Size(75, 23)
        Me.BtnScan.TabIndex = 3
        Me.BtnScan.Text = "&Scan"
        Me.BtnScan.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.UnhideChk)
        Me.GroupBox1.Controls.Add(Me.RemVBS)
        Me.GroupBox1.Controls.Add(Me.RemAutoruns)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(286, 126)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clean Options"
        '
        'UnhideChk
        '
        Me.UnhideChk.AutoSize = True
        Me.UnhideChk.Checked = True
        Me.UnhideChk.CheckState = System.Windows.Forms.CheckState.Checked
        Me.UnhideChk.Location = New System.Drawing.Point(15, 97)
        Me.UnhideChk.Name = "UnhideChk"
        Me.UnhideChk.Size = New System.Drawing.Size(84, 17)
        Me.UnhideChk.TabIndex = 2
        Me.UnhideChk.Text = "Unhide Files"
        Me.UnhideChk.UseVisualStyleBackColor = True
        '
        'RemVBS
        '
        Me.RemVBS.AutoSize = True
        Me.RemVBS.Checked = True
        Me.RemVBS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.RemVBS.Location = New System.Drawing.Point(15, 74)
        Me.RemVBS.Name = "RemVBS"
        Me.RemVBS.Size = New System.Drawing.Size(125, 17)
        Me.RemVBS.TabIndex = 1
        Me.RemVBS.Text = "Remove VBS Scripts"
        Me.RemVBS.UseVisualStyleBackColor = True
        '
        'RemAutoruns
        '
        Me.RemAutoruns.AutoSize = True
        Me.RemAutoruns.Checked = True
        Me.RemAutoruns.CheckState = System.Windows.Forms.CheckState.Checked
        Me.RemAutoruns.Location = New System.Drawing.Point(15, 51)
        Me.RemAutoruns.Name = "RemAutoruns"
        Me.RemAutoruns.Size = New System.Drawing.Size(111, 17)
        Me.RemAutoruns.TabIndex = 0
        Me.RemAutoruns.Text = "Remove Autoruns"
        Me.RemAutoruns.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AppStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 418)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(310, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'AppStatus
        '
        Me.AppStatus.Name = "AppStatus"
        Me.AppStatus.Size = New System.Drawing.Size(39, 17)
        Me.AppStatus.Text = "Status"
        '
        'detectedList
        '
        Me.detectedList.FormattingEnabled = True
        Me.detectedList.Location = New System.Drawing.Point(12, 235)
        Me.detectedList.Name = "detectedList"
        Me.detectedList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.detectedList.Size = New System.Drawing.Size(286, 147)
        Me.detectedList.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.detectedList, "Select all the files that you want to remove. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Drag the mouse or use [SHIFT] or " &
        "[CTRL] key for multiple selection")
        '
        'BtnClean
        '
        Me.BtnClean.Location = New System.Drawing.Point(223, 392)
        Me.BtnClean.Name = "BtnClean"
        Me.BtnClean.Size = New System.Drawing.Size(75, 23)
        Me.BtnClean.TabIndex = 8
        Me.BtnClean.Text = "Clean"
        Me.BtnClean.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 219)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Detected Files (Select to remove)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 20000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        Me.ToolTip1.ShowAlways = True
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTip1.ToolTipTitle = "File Removal"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(310, 24)
        Me.MenuStrip1.TabIndex = 10
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RefreshDeviceListToolStripMenuItem, Me.ShowHiddenFilesToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'RefreshDeviceListToolStripMenuItem
        '
        Me.RefreshDeviceListToolStripMenuItem.Image = Global.Shortcleaner.My.Resources.Resources.usbpng
        Me.RefreshDeviceListToolStripMenuItem.Name = "RefreshDeviceListToolStripMenuItem"
        Me.RefreshDeviceListToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.RefreshDeviceListToolStripMenuItem.Text = "Refresh Device List"
        '
        'ShowHiddenFilesToolStripMenuItem
        '
        Me.ShowHiddenFilesToolStripMenuItem.CheckOnClick = True
        Me.ShowHiddenFilesToolStripMenuItem.Image = Global.Shortcleaner.My.Resources.Resources.eyepng
        Me.ShowHiddenFilesToolStripMenuItem.Name = "ShowHiddenFilesToolStripMenuItem"
        Me.ShowHiddenFilesToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ShowHiddenFilesToolStripMenuItem.Text = "Show Hidden Files"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem1})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.AboutToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem1
        '
        Me.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1"
        Me.AboutToolStripMenuItem1.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem1.Text = "About"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.Shortcleaner.My.Resources.Resources.refreshicon
        Me.PictureBox1.Location = New System.Drawing.Point(278, 47)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 21)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'MainWindows
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(310, 440)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnClean)
        Me.Controls.Add(Me.detectedList)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BtnScan)
        Me.Controls.Add(Me.DeviceLabel)
        Me.Controls.Add(Me.ComboBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MainWindows"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Shortcut Cleaner"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents DeviceLabel As Label
    Friend WithEvents BtnScan As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents UnhideChk As CheckBox
    Friend WithEvents RemVBS As CheckBox
    Friend WithEvents RemAutoruns As CheckBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents AppStatus As ToolStripStatusLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents detectedList As ListBox
    Friend WithEvents BtnClean As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RefreshDeviceListToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowHiddenFilesToolStripMenuItem As ToolStripMenuItem
End Class
