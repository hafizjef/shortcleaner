# [WIP] Shortcleaner

## Status :
`Work in progress`

## Screens :

![Screenshot](Assets/BETA1.png "BETA Screenshot")

## What it is :
- A Simple USB scanner
    - Filter based file detection
    - Simple removal process
    - System [__AUTOPLAY__](https://en.wikipedia.org/wiki/AutoPlay "AUTOPLAY Wikipedia") toggle using Registry

## What its not :
- USB-Detection auto scan _(Not yet)_
- Advance runtime antivirus 
- Background filewatcher

## TODO :
- [ ] Background service
- [ ] On-the-fly USB Detection and scanning
- [ ] Advance deletion and file unlocking
- [ ] Malicious process identification and removal

## Why? :
> _The question was **never** why, the question is always **How**_

## Dependencies :
- [Microsoft .NET Framework 2.0](https://www.microsoft.com/en-us/download/details.aspx?id=1639)